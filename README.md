Deep Profiler Controller Setup
==============================

This repo contains [Ansible](http://docs.ansible.com) playbooks
for the configuring the OS installation on the Deep Profiler
Controller (DPC).

The DPC is a [Technologic Systems](http://www.embeddedarm.com)
TS-4200/8160 compact computer running Debian Linux (version 6).
The vendor's OS installation needs some customization before
it can be used for the project.

## Installation

You will need to be familiar with Ansible before trying this.

1. Clone this repo to the system that serves as your Ansible control
machine.
2. Log-in to the new board via the system console.
3. Set a password for the root account.
4. Bring down the ethernet interface, edit the network configuration file
to use DHCP, then bring the interface back up.

```
ifdown eth0
vi /etc/network/interfaces
ifup eth0
```

5. Create a set of SSH host-keys on the DPC and restart the SSH server.

```
ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa
/etc/init.d/ssh restart
```

6. Configure the root account on the DPC to allow SSH logins with a
public key and add that public key to `~root/.ssh/authorized_keys`.
7. Edit `/etc/apt/sources.list` and change the URL for the Debian
repository to:

```
http://archive.debian.org/debian-archive/debian
```

8. Install Python on the DPC

```
apt-get update
apt-get install python-dev
```

9. From the repo directory, create an inventory file with the
following contents, replacing *HOSTNAME* with the desired host-name
for the DPC and *IPADDR* with its IP address.

```
[dpc]
HOSTNAME ansible_ssh_host=IPADDR hostname=HOSTNAME
```

10. Run Ansible from the repo directory.

```
ansible-playbook -i inventory_file provision.yaml
```

## Additional Software

The application software components that are written in Python and BASH
are downloaded to the DPC using Git. The `mr` tool is used to manage the
multiple repositories. Log-in to the `rsn` account an run the command:

```
mr update
```

This will download the latest versions. Further instructions for each
repository follow below.

### Configuration Files

[Repository link](https://bitbucket.org/mfkenney/dp-config/src)

```
cd ~/src/dp-config
make dpc
```

### Utility Scripts

[Repository link](https://bitbucket.org/mfkenney/dp-utils/src)

```
cd ~/src/dp-utils
make dpc
```

### Kermit Scripts

[Repository link](https://bitbucket.org/mfkenney/dpc-kermit/src)

```
cd ~/src/dpc-kermit
cp -v *.ksc ~/kermit/
```

### Service Scripts

[Repository link](https://bitbucket.org/mfkenney/dpc-services/src)

```
cd ~/src/dpc-services
make install-sv install-bin
```

### Python Package

[Repository link](https://bitbucket.org/mfkenney/rsn-pydp/src)

```
cd ~/src/rsn-pydp
pip install -e .
```
